package kr.co.hbilab.app;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class OracleDAO implements Dao{
    private JdbcTemplate jdbcTemplate;
    
    StringBuffer sql = new StringBuffer();
    PreparedStatement pstmt;
    ResultSet rs;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<DeptDTO> selectAll() {
        
        RowMapper<DeptDTO> rm = new RowMapper<DeptDTO>(){
            @Override
            public DeptDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                int deptno = rs.getInt("deptno");
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");
                
                DeptDTO dto = new DeptDTO(deptno, dname, loc);
                return dto;
            }
        };

        sql.setLength(0);
        sql.append("\n SELECT * FROM DEPT ");
        List<DeptDTO> list = jdbcTemplate.query(sql.toString(), rm);
        
        return list;
    }

    @Override
    public DeptDTO selectOne(int no) {
        RowMapper<DeptDTO> rm = new RowMapper<DeptDTO>(){
            @Override
            public DeptDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                int deptno = rs.getInt("deptno");
                String dname = rs.getString("dname");
                String loc = rs.getString("loc");
                
                DeptDTO dto = new DeptDTO(deptno, dname, loc);
                return dto;
            }
        };
        
        sql.setLength(0);
        sql.append("\n SELECT * FROM DEPT ");
        sql.append("\n  WHERE DEPTNO = ?  ");
        DeptDTO dto = jdbcTemplate.queryForObject(sql.toString(), rm, no);
        return dto;
    }

    @Override
    public void indertOne(DeptDTO dto) {
        sql.setLength(0);
        sql.append("\n INSERT INTO DEPT(deptno, dname, loc) ");
        sql.append("\n VALUES( ?, ?, ?) ");
        
        int result = jdbcTemplate.update(sql.toString(), dto.getDeptno(), dto.getDname(), dto.getLoc());
        if(result>0){
            System.out.println("등록완료");
        }else{
            System.out.println("등록실패");
        }
    }

    @Override
    public void updateOne(DeptDTO dto) {
        sql.setLength(0);
        sql.append("\n UPDATE DEPT SET       ");
        sql.append("\n    DNAME = ?, LOC = ? ");
        sql.append("\n  WHERE DEPTNO = ?     ");
        
        int result = jdbcTemplate.update(sql.toString(), dto.getDname(), dto.getLoc(), dto.getDeptno());
        if(result>0){
            System.out.println("수정완료");
        }else{
            System.out.println("수정실패");
        }
    }

    @Override
    public void deleteOne(int no) {
        sql.setLength(0);
        sql.append("\n DELETE FROM DEPT  ");
        sql.append("\n  WHERE DEPTNO = ? ");
        int result = jdbcTemplate.update(sql.toString(), no);
        if(result>0){
            System.out.println("삭제완료");
        }else{
            System.out.println("삭제실패");
        }
    }
    
}
