package spring_app10_anno;

public class Sender {
    public void show(){
        System.out.println("Sender 클래스의 show() 메서드입니다");
        System.out.println("Sender 객체의 참조값 : " + this);
    }
}
