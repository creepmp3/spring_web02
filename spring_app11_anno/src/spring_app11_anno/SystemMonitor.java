package spring_app11_anno;


public class SystemMonitor implements Monitor{
    
    Sender sender;
    
    public SystemMonitor(){
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public SystemMonitor(Sender sender) {
        super();
        this.sender = sender;
    }

    @Override
    public void showMonitor() {
        if(sender!=null){
            sender.show();
        }
    }
    
}
