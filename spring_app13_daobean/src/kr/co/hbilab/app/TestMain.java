package kr.co.hbilab.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TestMain {
    public static void main(String[] args) {
        ApplicationContext ctx = new GenericXmlApplicationContext("app.xml");
        Dao dao = ctx.getBean("oracle", Dao.class);
        int cnt = dao.selectCount();
        System.out.println("부서의 총갯수 : " + cnt);
        ((AbstractApplicationContext)ctx).close();
    }
}
